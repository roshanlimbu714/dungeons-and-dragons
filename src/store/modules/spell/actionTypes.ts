export const SET_SPELLS = "SET_SPELLS";
export const SET_SPELL = "SET_SPELL";
export const SET_FAVORITES = "SET_FAVORITES";
